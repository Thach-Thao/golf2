<!DOCTYPE html>
<html lang="ja" id="pagetop">
<head>
<meta charset="UTF-8">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/meta.php'); ?>
<link rel="stylesheet" type="text/css" href="/assets/js/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="/assets/js/slick/slick-theme.css"/>
<link rel="stylesheet"  href="/assets/js/lightslider/lightslider.css"/>

<link href="/assets/css/style.css" rel="stylesheet">

</head>

<body class="page-<?php echo $id; ?>">

<header>
	<div class="c-headerPC">
		<div class="c-headerPC__MV">
			<p class="c-headerPC__banner">
				<img src="/assets/image/common/h_img01.png" width="930" height="344" alt="">
			</p>
		</div>
		<nav class="c-nav">
			<ul>
				<li><a href="">キャンペーン情報</a></li>
				<li><a href="">ご利用料金</a></li>
				<li><a href="">ご予約</a></li>
				<li><a href="">講師紹介</a></li>
				<li><a href="">店舗のご案内</a></li>
			</ul>
		</nav>
	</div>
</header>