<?php $id="index";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>

<div class="p-index">
	<div class="p-index01">
		<div class="l-container">
			<div class="p-index01__cont">
				<div class="c-box">
					<div class="c-box__num">
						<img src="/assets/image/index/No1.png" width="85" height="85">
					</div>
					<div class="c-box__txt01">
						事前お申込キャンペーン
					</div>
					<div class="c-box__txt02">
						
					</div>
					<div class="c-box__txt03">
						
					</div>
				</div>

				<div class="c-box">
					<div class="c-box__num">
						<img src="/assets/image/index/No2.png" width="85" height="85">
					</div>
					<div class="c-box__txt01">
						グランドオープン<br>入会キャンペーン
					</div>
					<div class="c-box__txt02">
						2018年<span>4月中</span>
					</div>
					<div class="c-box__txt03">
						入会金<span>無料！</span><br>
						4～５月分<span>3,000円（税抜）！！</span>
					</div>
				</div>
			</div>
			<div class="p-index01__btn">
				<a href="">見学・体験レッスンのご予約はこちら</a>
			</div>
		</div>
	</div>

	<div class="p-index02">
		<div class="l-container">
			<div class="p-index02__title">
				<img src="/assets/image/index/title01.png" width="379" height="68">
			</div>

			<div class="p-index02__cont">
				<div class="p-index02__ttl">
					事前申し込み＜期間限定＞
				</div>
				<div class="p-index02__cont--img">
					<p class="u-lf"><img src="/assets/image/index/img01.png" width="492" height="62" alt=""></p>
					<p><img src="/assets/image/index/img02.png" width="495" height="62" alt=""></p>
				</div>
			</div>

			<div class="p-index02__cont01">
				<div class="p-index02__ttl">
					通常料金 ※(税抜)
				</div>
				<div  class="p-index02__tbl">
					<table>
						<tr>
							<th></th>
						    <th>会員</th>
						    <th>ビジター</th> 
						    <th>フリー練習費</th>
						 </tr>
						<tr>
						    <td>⼊会⾦</td>
						    <td>￥5,000／1回</td>
						    <td>−</td>
						    <td>−</td>
						</tr>
						<tr>
						    <td>
						    	<p>レギュラー</p>
						    	<p>全⽇利⽤可</p>
						    </td>
						    <td>￥10,000（税抜）／⽉額</td>
						    <td>−</td>
						    <td class="u-red">無料</td>
						</tr>
						<tr>
						    <td>
						    	<p>デイタイム</p>
						    	<p>平⽇昼限定（11:00〜17:00）</p>
						    </td>
						    <td>￥7,000（税抜）／⽉額</td>
						    <td>−</td>
						    <td>時間外有料</td>
						</tr>
						<tr>
						    <td>
						    	<p>ジュニア</p>
						    	<p>⽊曜⽇限定 17：00〜18：00<br>中学3年生まで</p>
						    </td>
						    <td>￥6,000（税抜）／⽉額</td>
						    <td>−</td>
						    <td>時間外有料</td>
						</tr>
						<tr>
						    <td>
						    	<p>ホリデイ</p>
						    	<p>⼟・⽇・祝のみ終⽇利⽤可</p>
						    </td>
						    <td>￥8,000（税抜）／⽉額</td>
						    <td>−</td>
						    <td>時間外有料</td>
						</tr>
						<tr>
						    <td>
						    	<p>レッスン4</p>
						    	<p>レッスン4回<br>
						    	(スタンプカード制／有効期間：3ヶ月)</p>
						    </td>
						    <td>−</td>
						    <td>￥8,000（税抜）／1セット</td>
						    <td>有料</td>
						</tr>
						<tr>
						    <td>
						    	<p>フリー練習</p>
						    	<p>空打席があれば利⽤可<br>
						    	※レッスンは含まず</p>
						    </td>
						    <td>
						    	<p class="u-strikethrough">￥1,000（税抜）／1回</p><br>
						    	<p class="u-red">プレオープン期間限定 ￥0</p>
						    </td>
						     <td>
						    	<p class="u-strikethrough">￥1,500（税抜）／1回</p><br>
						    	<p class="u-red">プレオープン期間限定 ￥0</p>
						    </td>
						    <td>有料</td>
						</tr>
						<tr>
						    <td>
						    	<p>体験レッスン</p>
						    	<p>全⽇利⽤可</p>
						    </td>
						    <td>−</td>
						     <td>￥2,000（税抜）／1回</td>
						    <td>−</td>
						</tr>
					</table>
				</div>
				<div class="p-index02__txt">
					<p class="p-index02__txt--greem">レッスン・フリー利用可能時間</p>
					<p>1時間（準備時間含む、会員・ビジター問わず）</p>
					<p class="p-index02__txt--blue">学生割引</p>
					<p>上記料金より50％OFF（会員カテゴリのみ適用※フリー練習含まず）</p>
				</div>
			</div>
		</div>
	</div>

	<div class="p-index03">
		<div class="l-container">
			<div class="p-index03__txt01">
				見学・体験のご予約もこちらから!
			</div>
			<div class="p-index03__txt02">
				Reservation<span>レッスンWeb予約</span>
			</div>
			<div class="p-index03__txt03">
				当スクールは予約制となっております。<br>お電話、または予約システムよりご予約をお願い致します。
			</div>
			<div class="p-index03__grbtn">
				<a href="" class="p-index03__btn01">ご予約はこちら</a>
				<a href="" class="p-index03__btn02">
					ご予約はこちら<br>
					<span>0493ｰ23ｰ8015</span>
				</a>
			</div>
		</div>
	</div>
</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>